﻿//
//	Author: Ilya Napolov
//	Github: @napolinkinpark3r
//	email: napolinkinpark3r@gmail.com
//
Shader "Learn_Package/_Noise Belnding Shader" {
	Properties {
		//Main
		_MainColor ("Tint", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_BumpTex ("Normal", 2D) = "bump" {}
		_BumpInt ("Bump Intensity", Range(0,1)) = 1
		//Noise
		_Noise ("Noise Texture", 2D) = "white" {}
		_NoiseBump ("Noise Normal", 2D) = "bump" {}
		_NoiseOffset ("Noise Offset", Vector) = (1,1,0,0)
		_NoiseInt ("Noise Intensity", Range(0,1)) = 0
		_NoiseRange("Noise Range", Range (0,1)) = 0
		//Parameters
		_BrightController ("Bright Controller", Range(0,1)) = 1
		_SpecColor ("Specular Color", Color) = (1,1,1,1)
		_SpecPower ("Specular Power", Range(0,1)) = 0.5
		_GlossPower ("Gloss Power", Range(0,1)) = 1 
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf BlinnPhong
		#pragma target 3.0
		
		//Main Parameters
		fixed4 _MainColor;
		sampler2D _MainTex;		
		sampler2D _BumpTex;
		fixed _BumpInt;
		
		//Noise Paramters
		sampler2D _Noise;
		sampler2D _NoiseBump;
		half4 _NoiseOffset;
		fixed _NoiseInt;
		fixed _NoiseRange;
		
		//Specular Parameter
		fixed _BrightController;
		fixed _SpecPower;
		fixed _GlossPower;
		
		struct Input {
			float2 uv_MainTex;
			//float2 uv_BumpTex;
			float2 uv_Noise;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			
			fixed4 white = fixed4(1,1,1,1);
			
			fixed4 main = tex2D(_MainTex, IN.uv_MainTex);
			fixed4 bump = tex2D(_BumpTex, IN.uv_MainTex);
			
			half2 coord = IN.uv_MainTex / _NoiseOffset.xy - _NoiseOffset.zw;
			fixed4 noise = lerp (white, tex2D (_Noise, coord), _NoiseInt);
			fixed4 noiseBump = lerp (white, tex2D (_NoiseBump, coord), _NoiseInt);

			fixed4 finalColor = main / lerp(white, noise, _NoiseRange) - _BrightController;			
			fixed3 finalBump = UnpackNormal(bump * lerp(white, noiseBump, _NoiseRange));
			finalBump = fixed3 (finalBump.x * _BumpInt,finalBump.y * _BumpInt, finalBump.z);
			
			o.Albedo = finalColor.rgb;
			o.Alpha = finalColor.a;
			o.Normal = finalBump;
			
			o.Specular = _SpecPower;
			o.Gloss = _GlossPower;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
